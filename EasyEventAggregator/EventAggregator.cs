﻿using System;

namespace Spikes.MessageHub
{
    public sealed class EventAggregator : IEventAggregator
    {
        private readonly Subscriptions _subscriptions;
        private Action<Type, object> _globalHandler;
        private Action<Guid, Exception> _globalErrorHandler;

        public EventAggregator() => _subscriptions = new Subscriptions();

        public void RegisterGlobalHandler(Action<Type, object> onMessage)
        {
            EnsureNotNull(onMessage);
            _globalHandler = onMessage;
        }

        public void RegisterGlobalErrorHandler(Action<Guid, Exception> onError)
        {
            EnsureNotNull(onError);
            _globalErrorHandler = onError;
        }

        public void Publish<T>(T message)
        {
            var localSubscriptions = _subscriptions.GetTheLatestSubscriptions();
            var msgType = typeof(T);
            _globalHandler?.Invoke(msgType, message);

            foreach (var subscription in localSubscriptions)
            {
                if (!subscription.Type.IsAssignableFrom(msgType))
                {
                    continue;
                }

                try
                {
                    subscription.Handle(message);
                }
                catch (Exception e)
                {
                    _globalErrorHandler?.Invoke(subscription.Token, e);
                }
            }
        }

        public Guid Subscribe<T>(Action<T> action)
        {
            return Subscribe(action, TimeSpan.Zero);
        }

        public Guid Subscribe<T>(Action<T> action, TimeSpan throttleBy)
        {
            EnsureNotNull(action);
            return _subscriptions.Register(throttleBy, action);
        }

        public void Unsubscribe(Guid token)
        {
            _subscriptions.UnRegister(token);
        }

        public bool IsSubscribed(Guid token)
        {
            return  _subscriptions.IsRegistered(token);
        }

        public void ClearSubscriptions()
        {
            _subscriptions.Clear(false);
        }

        public void Dispose()
        {
            _globalHandler = null;
            _globalErrorHandler = null;
            _subscriptions.Clear(true);
        }

        private static void EnsureNotNull(object obj)
        {
            if (obj is null) { throw new NullReferenceException(nameof(obj)); }
        }
    }
}