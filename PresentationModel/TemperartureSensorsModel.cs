﻿
namespace Spikes.SensorApp.PresentationModel{

    public class TemperatureSensorsModel : NotifyModelBase
    {
        private int _selected;

        public TemperatureSensorsModel(SensorValueType valueType, string name, string id, string location)
        {
            this.ValueType = valueType;
            Id = id;
            Location = location;
            SensorName = name;
            _selected = 0;
        }

        public SensorValueType ValueType { get; }

        public string SensorName { get; }

        public string Id { get; }

        public string Location { get; }

        public int Selected
        {
            get => _selected;
            set
            {
                var val = value == 1 ? 1: 0;
                if (_selected == val) return;
                _selected = val;
                OnPropertyChanged(nameof(Selected));

            }
        }
    }
}
