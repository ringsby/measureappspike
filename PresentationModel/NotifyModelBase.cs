﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Spikes.SensorApp.Properties;

namespace Spikes.SensorApp.PresentationModel
{
    public abstract  class NotifyModelBase: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
