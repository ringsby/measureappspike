﻿namespace Spikes.SensorApp.PresentationModel
{
    public enum SensorValueType
    {
        FromInt = 0,
        FromFloat = 1,
        FromDouble = 2
    }
}