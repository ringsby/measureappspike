﻿
namespace Spikes.SensorApp.PresentationModel
{
    public class TemperatureResultModel : NotifyModelBase
    {
        private double _temperature;

        public void Result(double val)
        {
            Temperature = val;
        }

        public string Id { get; set; }

        public string TempSensor { get; set; }

        public double Temperature
        {
            get => _temperature;
            set
            {
                _temperature = value;
                OnPropertyChanged(nameof(Temperature));
            }
        }
    }
}
