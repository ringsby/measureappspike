﻿
namespace Spikes.SensorApp.Contract
{
    public class TemperatureValue: IValueContract
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
        public static string DefaultId => "00";
    }
}
