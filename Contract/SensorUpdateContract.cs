﻿using System.Collections.Generic;

namespace Spikes.SensorApp.Contract
{
    public class SensorUpdateContract : ISensorUpdateContract
    {
        public IEnumerable<ISensorContract> DiscoveredSensors { get; set; }

        public List<string> RemovedSensors { get; set; } = new List<string>();

        public List<string> AddedSensors { get; set; } = new List<string>();

        public bool TryGetAddedSensors(out List<string> sensorIds)
        {
            sensorIds = AddedSensors;
            return AddedSensors.Count>0;
        }

        public bool TryGetRemovedSensors(out List<string> sensorIds)
        {
            sensorIds = RemovedSensors;
            return RemovedSensors.Count > 0;
        }
    }
}