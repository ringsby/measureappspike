﻿using System.Collections.Generic;

namespace Spikes.SensorApp.Contract
{
    public interface ISensorUpdateContract
    {
        /// <summary>
        /// All senors available for this session
        /// </summary>
        IEnumerable<ISensorContract> DiscoveredSensors { get; set; }

        List<string> AddedSensors { get; }
        List<string> RemovedSensors { get; }

        bool TryGetAddedSensors(out List<string> sensorIds);
        bool TryGetRemovedSensors(out List<string> sensorIds);
    }
}