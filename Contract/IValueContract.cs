﻿namespace Spikes.SensorApp.Contract
{
    public interface IValueContract
    {
        string Id { get; set; }
        string Name { get; set; }
        double Value { get; set; }
    }
}