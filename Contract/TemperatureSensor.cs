﻿using System;

namespace Spikes.SensorApp.Contract
{
    public class TemperatureSensor : ISensorContract
    {
        public string Id { get; set; }
        public string SensorName { get; set; }
        public string Location { get; set; }
        public Type ValueType { get; set; }
    }
}
