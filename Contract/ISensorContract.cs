﻿using System;

namespace Spikes.SensorApp.Contract
{
    public interface ISensorContract
    {
        string Id { get; set; }
        string SensorName { get; set; }
        string Location { get; set; }
        Type ValueType { get; set; }
    }
}