﻿using System.Windows;
using System.Windows.Controls;

namespace Spikes.SensorApp.View
{
    /// <summary>
    /// Interaction logic for TemperatureView.xaml
    /// </summary>
    public partial class TemperatureView : UserControl
    {
        public TemperatureView()
        {
            InitializeComponent();
        }
    }
}
