﻿using System.Windows.Controls;

namespace Spikes.SensorApp.View
{
    /// <summary>
    /// Interaction logic for PreparePageView.xaml
    /// </summary>
    public partial class PreparePageView : UserControl
    {
        public PreparePageView()
        {
            InitializeComponent();
        }
    }
}
