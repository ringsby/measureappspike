﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Spikes.MessageHub;
using Spikes.SensorApp.ApplicationLogic;
using Spikes.SensorApp.ApplicationLogic.MeasurementLogic;
using Spikes.SensorApp.Data;

namespace Spikes.SensorApp.Presentation
{
    public class MainWindowPresenter: INotifyPropertyChanged
    {
        private IPagePresenter _currentPagePresenter;
        private List<IPagePresenter> _pagePresenters;
        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindowPresenter()
        {
            IEventAggregator ea = new EventAggregator();
            ISensorStorage fakeSensorStorage = new FakeSensorStorage();
            ISensorTypeFilter sensorTypeFilter = new SensorTypeFilter(new List<SensorSubscriptionType>()
                {
                    SensorSubscriptionType.SimpleTempSensor
                });
            ISensorLogic logic = new TemperatureSensorLogic(
                ea, 
                fakeSensorStorage,
                new FakeValueStorage(), 
                new FakeSensorSubSystem(ea,sensorTypeFilter),
                () => new MeasurementOperationFactory().GetMeasurementOperation(ea, MeasurementOperationTypes.MeanValue), 
                "1"
            );

            // Add available pages and set page
            PagePresenters.Add(new PreparePresenter(ea, logic));
            PagePresenters.Add(new TemperaturePresenter(new AppUiContext(), ea, logic));
            CurrentPagePresenter = PagePresenters[0];

            Mediator.Subscribe("GoToPrepareScreen", OnPrepareScreen);
            Mediator.Subscribe("GoToTemperatureScreen", OnTemperatureScreen);
        }

        public List<IPagePresenter> PagePresenters => _pagePresenters ?? (_pagePresenters = new List<IPagePresenter>());

        public IPagePresenter CurrentPagePresenter
        {
            get => _currentPagePresenter;
            set
            {
                _currentPagePresenter = value;
                OnPropertyChanged(nameof(CurrentPagePresenter));
                
            }
        }
        
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ChangePresenter(IPagePresenter presenter)
        {
            if (!PagePresenters.Contains(presenter))
                PagePresenters.Add(presenter);
            
            presenter.OnLoad();
            CurrentPagePresenter = PagePresenters.FirstOrDefault(vm => vm == presenter);
        }

        private void OnPrepareScreen(object obj)
        {
            ChangePresenter(PagePresenters[0]);
        }

        private void OnTemperatureScreen(object obj)
        {
            ChangePresenter(PagePresenters[1]);
        }
    }
}
