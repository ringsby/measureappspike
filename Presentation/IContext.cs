﻿using System;

namespace Spikes.SensorApp.Presentation
{
    public interface IContext
    {
        bool IsSynchronized { get; }
        void Invoke(Action action);
        void BeginInvoke(Action action);
    }
}