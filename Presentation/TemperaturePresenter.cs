﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Spikes.MessageHub;
using Spikes.SensorApp.ApplicationLogic;
using Spikes.SensorApp.Contract;
using Spikes.SensorApp.PresentationModel;

namespace Spikes.SensorApp.Presentation
{
    public class TemperaturePresenter: INotifyPropertyChanged, IPagePresenter
    {
        private string _buttonContent;
        private readonly IEventAggregator _ea;
        private readonly ISensorLogic _logic;
        private bool _started;

        // Using a wrapper around Dispatcher to be able to unit test.
        // Wanted to use another approach than in "PreparePresenter".
        private readonly IContext _context;

        public event PropertyChangedEventHandler PropertyChanged;

        public TemperaturePresenter(IContext context,IEventAggregator ea, ISensorLogic logic)
        {
            _context = context;
            _ea = ea;
            _logic = logic;
            InitCommands();
            _buttonContent = "Start sensor";
        }
        
        public ObservableCollection<TemperatureResultModel> Temperatures { get; set; }

        public string ButtonContent
        {
            get => _buttonContent;
            set
            {
                _buttonContent = value;
                OnPropertyChanged(nameof(ButtonContent));
            }
        }

        public void OnLoad()
        {
            LoadAvailableTemperatures();
            SubscribeToResult();
            ToggleSensorReadingCommand.Execute(null);
        }

        public ICommand ToggleSensorReadingCommand { get; set; }

        public ICommand TakeTemperatureValueCommand { get; set; }


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void InitCommands()
        {
            ToggleSensorReadingCommand = new RelayCommand(StopStartSensorAction);
            TakeTemperatureValueCommand = new RelayCommand(TakeTemperatureValueAction);
        }

        private void LoadAvailableTemperatures()
        {
            var result = _logic.GetSensors();
            Temperatures = new ObservableCollection<TemperatureResultModel>();
            foreach (var temperature in result.Select(sensorContract => new TemperatureResultModel() {
                Id = sensorContract.Id,TempSensor = $"{sensorContract.SensorName} at {sensorContract.Location}", Temperature = -273
            }))
            {
                Temperatures.Add(temperature);
            }
        }

        private void TakeTemperatureValueAction(object obj)
        {
            var result = _logic.GetAllReadings();
            var str = new StringBuilder();
            foreach (var valueContract in result)
            {
                str.Append(valueContract.Value);
                str.Append($" with id={valueContract.Id}");
                str.AppendLine();
            }
            MessageBox.Show(str.ToString());
        }


        private void StopStartSensorAction(object obj)
        {
            if (_started)
            {
                foreach (var temperatureResultModel in Temperatures)
                {
                    _logic.DeactivateSensor(temperatureResultModel.Id);
                    _started = false;
                }
                ButtonContent = "Start sensor";
            }
            else
            {
                foreach (var temperatureResultModel in Temperatures)
                {
                    _logic.ActivateSensor(temperatureResultModel.Id);
                    _started = true;
                }
               
                ButtonContent = "Stop Sensor";
            }
        }

        /// <summary>
        /// Handler ensures action is executed synchronously on gui thread
        /// </summary>
        private void SubscribeToResult()
        {
            _ea.Subscribe((IValueContract result)  =>
            {
                _context.Invoke(() =>
                {
                    Temperatures.FirstOrDefault(s => s.Id == result.Id)?.Result(result.Value);
                });
            });
        }
    }
}
