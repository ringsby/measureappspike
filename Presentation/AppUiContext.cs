﻿using System;
using System.Threading;
using System.Windows.Threading;

namespace Spikes.SensorApp.Presentation
{
    public class AppUiContext : IContext
    {

        private readonly Dispatcher _dispatcher;

        public AppUiContext() : this(Dispatcher.CurrentDispatcher)
        {

        }

        public AppUiContext(Dispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        public bool IsSynchronized => this._dispatcher.Thread == Thread.CurrentThread;

        public void Invoke(Action action)
        {
            this._dispatcher.Invoke(action);
        }

        public void BeginInvoke(Action action)
        {
            this._dispatcher.BeginInvoke(action);
        }
    }
}