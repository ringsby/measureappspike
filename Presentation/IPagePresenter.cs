﻿namespace Spikes.SensorApp.Presentation
{
    public interface IPagePresenter
    {
        void OnLoad();

    }
}