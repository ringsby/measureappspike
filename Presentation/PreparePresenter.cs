﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Input;
using Spikes.MessageHub;
using Spikes.SensorApp.ApplicationLogic;
using Spikes.SensorApp.Contract;
using Spikes.SensorApp.PresentationModel;

namespace Spikes.SensorApp.Presentation
{
    public class PreparePresenter : INotifyPropertyChanged, IPagePresenter
    {
        private readonly ISensorLogic _logic;
        private readonly IEventAggregator _ea;

        //- Threads is like a box of chocolates. You never know what you're gonna get.
        // I have little experience using "SynchronizationContext" class and I don't know if usage is right.
        // But it works and it is possible to Unit test as is.
        private readonly SynchronizationContext _context;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<TemperatureSensorsModel> SensorCollection { get; set; }

        public PreparePresenter(IEventAggregator ea,ISensorLogic logic)
        { 
            _context = SynchronizationContext.Current;
            _ea = ea;
            _logic = logic;
            InitCommands();
            SensorCollection = new ObservableCollection<TemperatureSensorsModel>();
            SubscribeSensorsUpdates();
        }

        public void OnLoad()
        {
        }

        public ICommand ToggleSelected { get; set; }

        public ICommand TemperatureViewCommand { get; set; }


        private SensorValueType ParseValueType(Type valueValueType)
        {
            return SensorValueType.FromDouble;
        }

        private void InitCommands()
        {
            ToggleSelected = new RelayCommand(x => ToggleSelectedAction(x as TemperatureSensorsModel));
            TemperatureViewCommand = new RelayCommand(x =>
            {
                Mediator.Notify("GoToTemperatureScreen", "");
            });
        }

        private void ToggleSelectedAction(TemperatureSensorsModel sensor)
        {
            sensor.Selected = sensor.Selected == 0 ? 1 : 0;
            if (sensor.Selected == 0)
            {
                _logic.RemoveSensor(sensor.Id);
            }
            else
            {
                _logic.AddSensor(sensor.Id);
            }
        }

        private void SubscribeSensorsUpdates()
        {
            _ea.Subscribe((Action<SensorUpdateContract>) NewSensorHandler);
        }

        /// <summary>
        /// Handler ensures action is executed synchronously on gui thread
        /// </summary>
        /// <param name="sensorUpdate"></param>
        private void NewSensorHandler(SensorUpdateContract sensorUpdate)
        {
            _context.Post(InternalUpdateMethod, sensorUpdate);
        }

        private void InternalUpdateMethod(object state)
        {
            var sensorUpdate = state as SensorUpdateContract;

            if (sensorUpdate.TryGetAddedSensors(out var addedIds))
            {
                foreach (var sensorId in addedIds)
                {
                    var contract = sensorUpdate.DiscoveredSensors.FirstOrDefault(s => s.Id == sensorId);
                    if (contract == null) continue;
                    var sensor = new TemperatureSensorsModel(
                        ParseValueType(contract.ValueType),
                        contract.SensorName,
                        contract.Id,
                        contract.Location)
                    {
                        Selected = _logic.IsSelected(contract.Id) ? 1 : 0
                    };
                    ;
                    SensorCollection.Add(sensor);
                }
            }

            if (!sensorUpdate.TryGetRemovedSensors(out var removedIds)) return;
            {
                foreach (var removedId in removedIds)
                {
                    var sensor = SensorCollection.FirstOrDefault(s => s.Id == removedId);
                    if (sensor != default)
                    {
                        SensorCollection.Remove(sensor);
                    }
                }
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
