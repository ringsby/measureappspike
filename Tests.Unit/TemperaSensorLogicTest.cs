﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Spikes.MessageHub;
using Spikes.SensorApp.ApplicationLogic;
using Spikes.SensorApp.ApplicationLogic.MeasurementLogic;
using Spikes.SensorApp.Contract;
using Spikes.SensorApp.Data;

namespace Spikes.SensorApp.Tests.Unit
{
    [TestFixture]
    public class TemperaSensorLogicTest
    {
        private Mock<ISensor> _sensor1Mock, _sensor2Mock;
        private Mock<IEventAggregator> _eaMock;
        private Mock<ISensorStorage> _storageMock;
        private Mock<IValueStorage> _valueStorageMock;
        private Mock<ISensorController> _sensorControllerMock;
        private readonly string _sessionId = "1";
        private Func<IMeasurementOperation> _operationDelegate;
        private Mock<IMeasurementOperation> _measurementOperationMock;


        private void SetupSensorMock()
        {
            _sensor1Mock = new Mock<ISensor>();
            _sensor1Mock.Setup(s => s.Id).Returns("101");
            _sensor1Mock.Setup(s => s.SubscriptionType).Returns(SensorSubscriptionType.SimpleTempSensor);
            _sensor2Mock = new Mock<ISensor>();
            _sensor2Mock.Setup(s => s.Id).Returns("102");
            _sensor2Mock.Setup(s => s.SubscriptionType).Returns(SensorSubscriptionType.SimpleTempSensor);
            _sensorControllerMock = new Mock<ISensorController>();
            _sensorControllerMock.Setup(m => m.Sensors)
                .Returns(new List<ISensor>() {_sensor1Mock.Object, _sensor2Mock.Object});
        }

        private IMeasurementOperation OperationDelegate()
        {
            var valueMock = new Mock<IValueContract>();
            valueMock.Setup(m => m.Value).Returns(1.1);
            valueMock.Setup(m => m.Id).Returns("1");
            valueMock.Setup(m => m.Name).Returns("Test");
            _measurementOperationMock = new Mock<IMeasurementOperation>();
            _measurementOperationMock.Setup(m => m.GetValue(It.IsAny<string>())).Returns(valueMock.Object);
            _measurementOperationMock.Setup(m => m.TemperatureSensors).Returns(new Dictionary<string, ISensor>());
            return _measurementOperationMock.Object;
        }

        private TemperatureSensorLogic MakeLogicInstance()
        {
            return new TemperatureSensorLogic(_eaMock.Object, _storageMock.Object, _valueStorageMock.Object, _sensorControllerMock.Object, _operationDelegate, _sessionId);
        }

        [SetUp]
        public void Setup()
        {
            _valueStorageMock = new Mock<IValueStorage>();
            SetupSensorMock();
            _eaMock = new Mock<IEventAggregator>();
            _operationDelegate = OperationDelegate;
        }

        [Test]
        public void ShallAddSensorToCollections()
        {
            //Arrange
            var emptyList = new List<TemperatureSensor>();
            _storageMock = new Mock<ISensorStorage>();
            _storageMock.Setup(m => m.TryLoad(It.IsAny<string>(), out emptyList))
                .Returns(false);

            var logic = MakeLogicInstance();
            //Act
            var sensorCount = logic.AddSensor("101");
            //Assert
            Assert.AreEqual(1,sensorCount);
            _storageMock.Verify(s => s.Save<ISensorContract>(It.IsAny<List<ISensorContract>>(),It.IsAny<string>()));
        }

        [Test]
        public void ShallNotAddSensorToCollections()
        {
            //Arrange
            var emptyList = new List<TemperatureSensor>();
            _storageMock = new Mock<ISensorStorage>();
            _storageMock.Setup(m => m.TryLoad(It.IsAny<string>(), out emptyList))
                .Returns(false);

            var logic = MakeLogicInstance();
            //Act
            var sensorCount = logic.AddSensor("103");
            //Assert
            Assert.AreEqual(0, sensorCount);
            _storageMock.Verify(s => s.Save<ISensorContract>(It.IsAny<List<ISensorContract>>(), It.IsAny<string>()),Times.Never);
        }

        [Test]
        public void ShallRegisterSubscriptionOnlyOnce()
        {
            //Arrange
            var emptyList = new List<TemperatureSensor>();
            _storageMock = new Mock<ISensorStorage>();
            _storageMock.Setup(m => m.TryLoad(It.IsAny<string>(), out emptyList))
                .Returns(false);

            var logic = MakeLogicInstance();
            logic.AddSensor("101");
            //Act
            var sensorCount = logic.AddSensor("102");
            //Assert
            Assert.AreEqual(2, sensorCount);
            _eaMock.Verify(m => m.Subscribe(It.IsAny<Action<ISensorResult<double>>>()),Times.Exactly(1));
        }

        [Test]
        public void ActivateSensorShallReturnTrue()
        {
            //Arrange
            var emptyList = new List<TemperatureSensor>();
            _storageMock = new Mock<ISensorStorage>();
            _storageMock.Setup(m => m.TryLoad(It.IsAny<string>(), out emptyList))
                .Returns(false);
            var logic = MakeLogicInstance();
            logic.AddSensor("101");
            //Act
            var test = logic.ActivateSensor("101");
            //Assert
            Assert.True(test);
        }

        [Test]
        public void ShallReturnValueContract()
        {
            //Arrange
            var emptyList = new List<TemperatureSensor>();
            _storageMock = new Mock<ISensorStorage>();
            _storageMock.Setup(m => m.TryLoad(It.IsAny<string>(), out emptyList))
                .Returns(false);

            var logic = MakeLogicInstance();
            logic.AddSensor("101");
            //Act
            var test = logic.GetReading("101");
            //Arrange
            Assert.IsInstanceOf<IValueContract>(test);
        }
    }
}