﻿using System;
using Moq;
using NUnit.Framework;
using Spikes.MessageHub;
using Spikes.SensorApp.ApplicationLogic;
using Spikes.SensorApp.Contract;
using Spikes.SensorApp.Presentation;

namespace Spikes.SensorApp.Tests.Unit
{
    [TestFixture]
    internal sealed class PresenterTests
    {
        private Mock<IEventAggregator> _eaMock;
        private Mock<ISensorLogic> _sensorLogicMock;

        [SetUp]
        public void Setup()
        {
           _eaMock = new Mock<IEventAggregator>();
           _sensorLogicMock = new Mock<ISensorLogic>();
        }

        [Test]
        public void ShallSubscribeToSensorUpdates()
        {
            // Arrange
            //Act
            var presenter = new PreparePresenter(_eaMock.Object, _sensorLogicMock.Object);

            //Assert
            _eaMock.Verify( m=>m.Subscribe(It.IsAny<Action<SensorUpdateContract>>()),Times.Exactly(1));
        }
    }
}