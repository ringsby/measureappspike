﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Spikes.MessageHub;
using Spikes.SensorApp.Data;

namespace Spikes.SensorApp.Tests.Unit
{
    [TestFixture]
    internal sealed class TemperatureSensorTest
    {
        Mock<IEventAggregator> _eaMock;
        private const double NotSetValue = -273.00;

        [SetUp]
        public void setup()
        {
            _eaMock = new Mock<IEventAggregator>();
        }

        [Test]
        public void ReadMethodShallReturnDefault()
        {
            // Arrange 
            var expected = new DefaultSensorResult();
            var sensor = new SimpleTemperatureSensor(_eaMock.Object,"111", "TestSensor", "Test");
            //Act
            var result = sensor.Read<double>();
            //Assert
            Assert.AreEqual(result.Id, expected.Id);
            Assert.AreEqual(result.Value, expected.Value);
        }

        [Test]
        public void ReadMethodShallNotReturnDefault()
        {
            // Arrange 
            var notExpected = new DefaultSensorResult();
            var sensor = new SimpleTemperatureSensor(_eaMock.Object, "111", "TestSensor", "Test");
            //Act
            sensor.StartSensorAsync(TimeSpan.FromMilliseconds(0));
            Thread.Sleep(10);
            var result = sensor.Read<double>();
            sensor.StopSensor();
            //Assert
            Assert.AreNotEqual(result.Id,notExpected.Id);
            Assert.AreNotEqual(result.Value, notExpected.Value);
            Assert.AreEqual(result.Id, "111");
        }

        [Test]
        public void ShallKeepLastPublishedValueAfterStop()
        {
            // Arrange 
            var ea = new EventAggregator();
            var sensor = new SimpleTemperatureSensor(ea, "111", "TestSensor", "Test");
            double value = NotSetValue;
            var tcs = new TaskCompletionSource<bool>();
            ea.Subscribe((ISensorResult<double> result) =>
            {
                if (value.Equals(NotSetValue))  // Only change value once
                {
                    value = result.Value;
                    tcs.TrySetResult(true);
                }
            });
            //Act
            sensor.StartSensorAsync(TimeSpan.FromMilliseconds(10));
            tcs.Task.Wait();
            sensor.StopSensor();
            Thread.Sleep(TimeSpan.FromMilliseconds(250));
            var test = sensor.Read<double>();
            //Assert
            Assert.AreNotEqual(NotSetValue, value);
            Assert.AreEqual(value, test.Value);
        }
    }
}