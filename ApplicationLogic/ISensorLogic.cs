﻿using System.Collections.Generic;
using Spikes.SensorApp.Contract;

namespace Spikes.SensorApp.ApplicationLogic
{
    public interface ISensorLogic
    {
        string SessionId { get; }

        /// <summary>
        /// Returns all senors used in this session.
        /// </summary>
        /// <returns>A list of sensors used in this instance </returns>
        List<ISensorContract> GetSensors();

        bool IsSelected(string sensorId);

        /// <summary>
        /// Adds a sensor to this session.
        /// Starts a subscription on sensor data.
        /// Sensor must exists from subsystem
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int AddSensor(string id);

        /// <summary>
        /// Remove sensor from sensorStorage
        /// Stop subscription
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        void RemoveSensor(string id);


        // TODO
        /// Following two methods could possibly be changed to following signatures
        /// bool ActivateResult()  resp. void DeactivateResults()
        /// And always effect all sensors in session.

        /// <summary>
        /// Sensor will start publish values.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool ActivateSensor(string id);

        /// <summary>
        /// Sensor will stop publish value
        /// </summary>
        /// <param name="id"></param>
        void DeactivateSensor(string id);

        /// <summary>
        /// Get latest value from sensor
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IValueContract GetReading(string id);

        /// <summary>
        /// Get values from all sensors
        /// </summary>
        /// <returns></returns>
        IEnumerable<IValueContract> GetAllReadings();
    }
}