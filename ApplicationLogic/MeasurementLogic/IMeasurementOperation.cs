﻿using System.Collections.Generic;
using Spikes.SensorApp.Contract;
using Spikes.SensorApp.Data;

namespace Spikes.SensorApp.ApplicationLogic.MeasurementLogic
{
    public interface IMeasurementOperation
    {
        Dictionary<string, ISensor> TemperatureSensors { get; set; }

        IValueContract GetValue(string sensorId);

        IEnumerable<IValueContract> GetAllValues();
    }
}