﻿using System.Collections.Generic;
using Spikes.SensorApp.Contract;
using Spikes.SensorApp.Data;

namespace Spikes.SensorApp.ApplicationLogic.MeasurementLogic
{
    public class RawValueTemperatures : IMeasurementOperation
    {
        public Dictionary<string, ISensor> TemperatureSensors { get; set; }

        public RawValueTemperatures()
        {
            TemperatureSensors = new Dictionary<string, ISensor>();
        }

        public IValueContract GetValue(string sensorId)
        {
            var result = new TemperatureValue();
            if (TemperatureSensors.ContainsKey(sensorId))
            {
                var sensorValue = TemperatureSensors[sensorId].Read<double>();
                result.Id = sensorValue.Id;
                result.Name = TemperatureSensors[sensorId].Description;
                result.Value = sensorValue.Value;
            }

            return result;
        }

        public IEnumerable<IValueContract> GetAllValues()
        {
            var snapshots = new List<TemperatureValue>();
            foreach (var tempSensor in TemperatureSensors)
            {
                snapshots.Add(new TemperatureValue() { Id = tempSensor.Key, Name = tempSensor.Value.Description, Value = tempSensor.Value.Read<double>().Value });
            }
            return snapshots;
        }
    }
}