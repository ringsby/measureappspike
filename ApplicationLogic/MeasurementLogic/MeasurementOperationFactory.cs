﻿using Spikes.MessageHub;

namespace Spikes.SensorApp.ApplicationLogic.MeasurementLogic
{

    public enum MeasurementOperationTypes
    {
        None = 0,
        MeanValue = 1,
        RawValue = 2
    }

    public interface IMeasurementLogicFactory
    {
        IMeasurementOperation GetMeasurementOperation(IEventAggregator ea,  MeasurementOperationTypes operationsTypes);
    }

    public class MeasurementOperationFactory: IMeasurementLogicFactory 
    {
        public  IMeasurementOperation GetMeasurementOperation(IEventAggregator ea, MeasurementOperationTypes operationTypes = MeasurementOperationTypes.None)
        {
            switch (operationTypes)
            {
                case MeasurementOperationTypes.RawValue:
                    return new RawValueTemperatures();
                case MeasurementOperationTypes.MeanValue:
                    return new MeanValueTemperatures(ea);
                default:
                    return new RawValueTemperatures();
            }
        }
    }
}