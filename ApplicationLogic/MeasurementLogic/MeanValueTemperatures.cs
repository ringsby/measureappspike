﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Spikes.MessageHub;
using Spikes.SensorApp.Contract;
using Spikes.SensorApp.Data;

namespace Spikes.SensorApp.ApplicationLogic.MeasurementLogic
{
    public class MeanValueTemperatures : IMeasurementOperation
    {
        public Dictionary<string, ISensor> TemperatureSensors { get; set; }
        private readonly ConcurrentDictionary<string, Queue<double>> _valueDictionary;
        private int _numberOfValues = 5;

        public MeanValueTemperatures(IEventAggregator ea)
        {
            _valueDictionary = new ConcurrentDictionary<string, Queue<double>>();
            TemperatureSensors = new Dictionary<string, ISensor>();
            ea.Subscribe((Action<TemperatureValue>) TemperatureValueHandler);
        }

        public IValueContract GetValue(string sensorId)
        {
            var result = new TemperatureValue();
            if (TemperatureSensors.ContainsKey(sensorId))
            {
                if (_valueDictionary.TryGetValue(sensorId, out var q))
                {
                    var sensorValue = q.Sum()/_numberOfValues;
                    result.Id = sensorId;
                    result.Name = TemperatureSensors[sensorId].Description;
                    result.Value = sensorValue;
                }
            }
            return result;
        }

        public IEnumerable<IValueContract> GetAllValues()
        {
            var snapshots = new List<TemperatureValue>();
            foreach (var tempSensor in TemperatureSensors)
            {
                if (_valueDictionary.TryGetValue(tempSensor.Key, out var q))
                {
                    snapshots.Add(new TemperatureValue()
                    {
                        Id = tempSensor.Key, Name = tempSensor.Value.Description,
                        Value = q.Sum()/_numberOfValues
                    });
                }
            }
            return snapshots;
        }

        private void TemperatureValueHandler(TemperatureValue obj)
        {
            try
            {
                if (_valueDictionary.TryGetValue(obj.Id, out var q))
                {
                    if (q.Count == _numberOfValues)
                    {
                        q.Dequeue();
                    }

                    q.Enqueue(obj.Value);
                }
                else
                {
                    var tq = new Queue<double>();
                    tq.Enqueue(obj.Value);
                    _valueDictionary.TryAdd(obj.Id, tq);
                }
            }
            catch (Exception e)
            {
                //TODO add logging
            }
        }
    }
}
