﻿using System.Collections.Generic;
using Spikes.SensorApp.Contract;

namespace Spikes.SensorApp.ApplicationLogic
{
    public interface IValueStorage
    {
        void Add<T>(string sessionId, IEnumerable<T> value ) where T : IValueContract;

        IEnumerable<List<T>> GetValues<T>(string sessionId, int offset, int number) where T : IValueContract;
    }
}