﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Spikes.SensorApp.Contract;

namespace Spikes.SensorApp.ApplicationLogic
{
    public class FakeValueStorage : IValueStorage
    {

        private const string FileName = "value.json";
        private const string PathString = "Sessions";
        private readonly string _path;

        public FakeValueStorage()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            _path = Path.Combine(currentDirectory, PathString);
            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }
        }

        public void Add<T>(string sessionId, IEnumerable<T> value) where T : IValueContract
        {
            using (StreamWriter file = new StreamWriter(MakePath(sessionId), true))
            {
                var j = JsonConvert.SerializeObject(value);
                file.WriteLine(j);
            }
        }

        public IEnumerable<List<T>> GetValues<T>(string sessionId, int offset, int number) where T : IValueContract
        {
            var res = new List<List<T>>();
            try
            {
                var jsonStr = File.ReadAllLines(MakePath(sessionId));
                foreach (var s in jsonStr)
                {
                    res.Add(JsonConvert.DeserializeObject<List<T>>(s));
                }
            }
            catch (FileNotFoundException)
            {

            }

            if (res.Count > offset && offset * number != 0)
            {
                return res.Count >= offset + number ? res.GetRange(offset, number) : res.GetRange(offset, res.Count-offset);
            }

            return res;
        }

        private string MakePath(string sessionId)
        {
            return Path.Combine(_path, $"{sessionId}{FileName}");
        }
    }
}