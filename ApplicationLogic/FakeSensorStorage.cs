﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Spikes.SensorApp.Contract;

namespace Spikes.SensorApp.ApplicationLogic
{
    public class FakeSensorStorage : ISensorStorage
    {

        private const string FileName = ".json";
        private const string PathString = "Sessions";
        private readonly string _path;

        public FakeSensorStorage()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            _path = Path.Combine(currentDirectory, PathString);
            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }
        }

        public void Save<T>(List<T> data, string session) where T : ISensorContract
        {
            if (string.IsNullOrEmpty(session)) return;
            var filePath = GetFilename(session);
            if (data.Count == 0)
            {
                File.Delete(filePath);
            }
            else
            {
                using (StreamWriter file = new StreamWriter(filePath, false))
                {
                    foreach (var sensorContract in data)
                    {
                        file.WriteLine(JsonConvert.SerializeObject(sensorContract));
                    }
                }
            }
        }

        public bool TryLoad<T>(string sessionId, out List<T> data) where T : ISensorContract
        {
            if (string.IsNullOrEmpty(sessionId))
            {
                data = new List<T>();
                return false;
            }

            var filePath = GetFilename(sessionId);
            try
            {
                data = new List<T>();
                var jsonStr = File.ReadAllLines(filePath);
                foreach (var s in jsonStr)
                {
                    data.Add(JsonConvert.DeserializeObject<T>(s));
                }

                return true;
            }
            catch (FileNotFoundException)
            {
                data = null;
                return false;
            }
        }

        private string GetFilename(string sessionId)
        {
            return Path.Combine(_path, $"{sessionId}{FileName}");
        }
    }
}
