﻿using System.Collections.Generic;
using Spikes.SensorApp.Contract;

namespace Spikes.SensorApp.ApplicationLogic
{
    public interface ISensorStorage
    {
        void Save<T>(List<T> data, string session) where T: ISensorContract;
        bool TryLoad<T>(string sessionId, out List<T> data) where T : ISensorContract;
    }
}