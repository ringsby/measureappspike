﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Spikes.MessageHub;
using Spikes.SensorApp.ApplicationLogic.MeasurementLogic;
using Spikes.SensorApp.Contract;
using Spikes.SensorApp.Data;

namespace Spikes.SensorApp.ApplicationLogic
{
    public class TemperatureSensorLogic : ISensorLogic
    {
        private readonly IEventAggregator _ea;
        private readonly ISensorStorage _sensorStorage;
        private readonly IValueStorage _valueStorage;
        private readonly IMeasurementOperation _measurementOperation;
        private readonly Dictionary<string, ISensor> _temperatureSensors;

        private readonly Dictionary<SensorSubscriptionType, Guid> _resultSubscriptions;
        private readonly ConcurrentDictionary<string, ISensor> _availableSensors;
    
        public TemperatureSensorLogic(
            IEventAggregator ea, 
            ISensorStorage sensorStorage,
            IValueStorage valueStorage,
            ISensorController sensorController,
            Func<IMeasurementOperation> setMeasurementLogic,
            string sessionId
            )
        {
            _ea = ea;
            _sensorStorage = sensorStorage;
            _valueStorage = valueStorage;
            _measurementOperation = setMeasurementLogic();
            _temperatureSensors = _measurementOperation.TemperatureSensors;

            _resultSubscriptions = new Dictionary<SensorSubscriptionType, Guid>();
            _availableSensors = new ConcurrentDictionary<string, ISensor>();

            SessionId = sessionId;
            SubscribeToSensorUpdates();
            foreach (var sensor in sensorController.Sensors.Where(s => _availableSensors.TryAdd(s.Id, s)))
            {
                AddToSessionIfPersisted(sensor.Id);
            }
            sensorController.ActivateSensorSystem();
        }               

        public string SessionId { get; }

        /// <inheritdoc/>
        public List<ISensorContract> GetSensors()
        {
            var sensorContracts =  ConvertToSensorContracts<TemperatureSensor>(_temperatureSensors.Values.ToList());
            if (!string.IsNullOrEmpty(SessionId))
            {
                _sensorStorage.Save(sensorContracts, SessionId);
            }
            return sensorContracts;
        }

        /// <inheritdoc/>
        public int AddSensor(string id)
        {
            var sensor = AddSensorToSession(id);
            if (sensor != default)
            {
                AddResultSubscriptionIfMissing(sensor);
                _sensorStorage.Save(
                    ConvertToSensorContracts<TemperatureSensor>(_temperatureSensors.Values.ToList()), SessionId);
            }

            return _temperatureSensors.Count;
        }

        /// <inheritdoc/>
        public void RemoveSensor(string id)
        {
            if (!_temperatureSensors.TryGetValue(id, out var sensor)) return;
            RemoveResultSubscriptionIfLastOfType(sensor.SubscriptionType);
            _temperatureSensors.Remove(id);
            _sensorStorage.Save(ConvertToSensorContracts<TemperatureSensor>(_temperatureSensors.Values.ToList()), SessionId);
        }

        /// <inheritdoc/>
        public bool ActivateSensor(string id)
        {
            if (_temperatureSensors.ContainsKey(id))
            {
                _temperatureSensors[id].StartSensorAsync(TimeSpan.FromMilliseconds(200));
                return true;
            }
            return false;
        }

        /// <inheritdoc/>
        public void DeactivateSensor(string id)
        {
            _temperatureSensors[id]?.StopSensor();
        }

        /// <inheritdoc/>
        public IValueContract GetReading(string id)
        {
            return _measurementOperation.GetValue(id);
        }

        public IEnumerable<IValueContract> GetAllReadings()
        {
            var valueContracts =  _measurementOperation.GetAllValues().ToList();
            _valueStorage.Add(SessionId, valueContracts);
            return valueContracts;
        }

        public bool IsSelected(string sensorId)
        {
            return _temperatureSensors.ContainsKey(sensorId);
        }

        private ISensor AddSensorToSession(string id)
        {
            lock (_temperatureSensors)
            {
                if (!_temperatureSensors.ContainsKey(id))
                {
                    if (_availableSensors.TryGetValue(id, out var sensor))
                    {
                        _temperatureSensors.Add(sensor.Id, sensor);
                        return sensor;
                    }
                }
                return default;
            }
        }

        /// <summary>
        /// subscribes to updates from sensor subsystem.
        /// </summary>
        private void SubscribeToSensorUpdates()
        {
            _ea.Subscribe((Action<List<ISensor>>) SensorUpdateHandler);
        }

        private void SensorUpdateHandler(List<ISensor> sensorList)
        {
            var removedSensors = _availableSensors.Keys.ToList().Except(sensorList.Select(s => s.Id)).ToList();
            var addedSensors = sensorList.Select(s => s.Id).Except(_availableSensors.Keys.ToList()).ToList();

            foreach (var sensor in sensorList.Where(s => _availableSensors.TryAdd(s.Id, s)))
            {
                AddToSessionIfPersisted(sensor.Id);
            }


            _ea.Publish(new SensorUpdateContract()
            {
                AddedSensors = addedSensors, 
                RemovedSensors = removedSensors,
                DiscoveredSensors = ConvertToSensorContracts<TemperatureSensor>(_availableSensors.Values.ToList())
            });
        }

        private void AddResultSubscriptionIfMissing(ISensor sensor)
        {
            switch (sensor.SubscriptionType)
            {
                case SensorSubscriptionType.SimpleTempSensor:
                    var key = SensorSubscriptionType.SimpleTempSensor;
                    if (!_resultSubscriptions.ContainsKey(key))
                    {
                        var subscription = _ea.Subscribe((Action<ISensorResult<double>>)SensorResultHandler);
                        _resultSubscriptions.Add(key, subscription);
                    }
                    break;
            }
        }

        private void RemoveResultSubscriptionIfLastOfType(SensorSubscriptionType subscriptionType)
        {
            var sensors = _temperatureSensors.Where(
                s => s.Value.SubscriptionType == subscriptionType
                );
            if (sensors.Count() == 1)
            {
                if (_resultSubscriptions.TryGetValue(subscriptionType, out var subscriptionGuid))
                {
                    _ea.Unsubscribe(subscriptionGuid);
                    _resultSubscriptions.Remove(subscriptionType);
                }
            }
        }

        private void SensorResultHandler(ISensorResult<double> sensorResult)
        {
            var valueContract = new TemperatureValue()
                { Id = sensorResult.Id, Value = sensorResult.Value, Name = _temperatureSensors[sensorResult.Id].Description };
            _ea.Publish(valueContract);
        }

        private void AddToSessionIfPersisted(string sensorId)
        {
            if (_sensorStorage.TryLoad<TemperatureSensor>(SessionId, out var dataList))
            {
                if (dataList.Exists(s => s.Id == sensorId))
                {
                    AddResultSubscriptionIfMissing(AddSensorToSession(sensorId));
                }
            }
        }

        private static List<ISensorContract> ConvertToSensorContracts<T>(IEnumerable<ISensor> sensors) where T : ISensorContract, new()
        {
            var sensorList = new List<ISensorContract>();
            foreach (var tempSensor in sensors)
            {
                var s = tempSensor;
                sensorList.Add(new T()
                {
                    Id = s.Id,
                    Location = s.Location,
                    SensorName = s.Description
                });
            }
            return sensorList;
        }
    }
}