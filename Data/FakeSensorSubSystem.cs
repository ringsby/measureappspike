﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Spikes.MessageHub;

namespace Spikes.SensorApp.Data
{
    public class FakeSensorSubSystem: ISensorController
    {
        private readonly IEventAggregator _ea;

        private readonly List<ISensor> _list = new List<ISensor>();

        public FakeSensorSubSystem(IEventAggregator ea, ISensorTypeFilter sensorTypeFilter)
        {
            _list.Add(new SimpleTemperatureSensor(ea, "101", "Temperature", "Kitchen"));
            Thread.Sleep(TimeSpan.FromMilliseconds(5));
            _list.Add(new SimpleTemperatureSensor(ea, "102", "Temperature", "Bedroom"));
            Thread.Sleep(TimeSpan.FromMilliseconds(5));
            _list.Add(new SimpleTemperatureSensor(ea, "103", "Temperature", "Living-room"));
            Thread.Sleep(TimeSpan.FromMilliseconds(5));
            _list.Add(new SimpleTemperatureSensor(ea, "104", "Temperature", "Bathroom"));
            Thread.Sleep(TimeSpan.FromMilliseconds(5));
            _list.Add(new SimpleTemperatureSensor(ea, "105", "Temperature", "Tv-room"));

            _ea = ea;
            Filter = sensorTypeFilter;
            Sensors = new List<ISensor>();
        }

        public List<ISensor> Sensors { get; }
        public ISensorTypeFilter Filter { get; }
        

        public void ActivateSensorSystem()
        {
            ActivateSensorSystemInternal();
        }

        private async void ActivateSensorSystemInternal()
        {
            await Task.Run(FakeSensors);
        }

        private void FakeSensors()
        {
            var rand = new Random(DateTime.Now.Millisecond);

            while (Sensors.Count<5)
            {
                var selector = rand.Next(0, 5);
                if (!Sensors.Exists(s => s.Id == _list[selector].Id))
                {
                    Sensors.Add(_list[selector]);
                }
                _ea.Publish(Sensors);
                Thread.Sleep(TimeSpan.FromMilliseconds(500));
            }
        }
    }
}