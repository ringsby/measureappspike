﻿using System.Collections.Generic;

namespace Spikes.SensorApp.Data
{
    public interface ISensorTypeFilter
    {
        IEnumerable<SensorSubscriptionType> SensorTypes { get; }
    }
}