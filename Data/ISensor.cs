﻿using System;
using System.Collections.Generic;

namespace Spikes.SensorApp.Data
{
    public interface ISmartSensorConfigSettings
    {
        List<int> Settings();
    }

    /// <summary>
    /// Provides a way to define false readings such as null or Nan.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISensorResult<T>
    {
        T Value { get; }
        string Id { get; }
        DateTime ValueTime { get; }
    }
    
    /// <summary>
    /// Representing a passive sensor. User may setup polling or pushing by will
    /// </summary>
    public interface ISensor
    {
        /// <summary>
        /// Sensor Id
        /// </summary>
        string Id { get; }

        /// <summary>
        /// Sensor description
        /// </summary>
        string Description { get; }
        
        /// <summary>
        /// Sensor location
        /// </summary>
        string Location { get; }

        SensorSubscriptionType SubscriptionType { get; }

        /// <summary>
        /// Provides access to latest result or a snapshot value 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        ISensorResult<T> Read<T>();

        /// <summary>
        /// Sets sensor into continuous report mode 
        /// </summary>
        /// <param name="throttleBy">Set report speed</param>
        void StartSensorAsync(TimeSpan throttleBy);

        /// <summary>
        /// Stop continuous reporting, Last value reported is still accessible
        /// </summary>
        void StopSensor();
    }

    public interface ISmartSensor: ISensor
    {
        bool TrySetConfig(ISmartSensorConfigSettings settings, out ISmartSensorConfigSettings currentConfigSettings);
        ISmartSensorConfigSettings CurrentSettings { get; }
    }
}
