﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Spikes.MessageHub;

namespace Spikes.SensorApp.Data
{

    public class SensorResult : ISensorResult<double>
    {

        public SensorResult(string id, double value)
        {
            this.Id = id;
            this.Value = value;
            ValueTime =DateTime.Now;
        }

        public string Id { get; }

        public double Value { get; }

        public DateTime ValueTime { get; }
    }

    public class DefaultSensorResult : ISensorResult<double>
    {
        public double Value => -273;
        public string Id => "00";
        public DateTime ValueTime => DateTime.Now;
    }

    public class SimpleTemperatureSensor : ISensor
    {
        private readonly IEventAggregator _eventAggregator;
        private CancellationTokenSource _cts;
        private CancellationToken _ct;
        private ISensorResult<double> _result;
        private readonly int _rand;

        public string Id { get; }
        public string Description { get; }
        public string Location { get; }


        public SensorSubscriptionType SubscriptionType => SensorSubscriptionType.SimpleTempSensor;

        public SimpleTemperatureSensor(IEventAggregator eventAggregator, string id, string description, string location)
        {
            this._eventAggregator = eventAggregator;
            Id = id;
            Description = description;
            Location = location;
            _result = new DefaultSensorResult();
            _rand = DateTime.Now.Millisecond;
        }

        public ISensorResult<T> Read<T>()
        {
            ISensorResult<double> res;
            lock (_result)
            {
                res = _result;
            }
            return (ISensorResult<T>) res;
        }

        public void StartSensorAsync(TimeSpan throttleBy)
        {
            _cts = new CancellationTokenSource();
            _ct = _cts.Token;
            var random = new Random(_rand);
            double val;
            Task.Run(() =>
            {
                while (!_ct.IsCancellationRequested)
                {
                    val = random.Next(17, 22) + (random.Next(0,9)*0.1);
                    ISensorResult<double> res = new SensorResult(Id, val);
                    lock (_result)
                    {
                        _result = res;
                    }
                    _eventAggregator.Publish(res);
                    Thread.Sleep(throttleBy);
                }
            }, _ct);
        }

        public void StopSensor()
        {
            _cts.Cancel();
        }
    }
}
