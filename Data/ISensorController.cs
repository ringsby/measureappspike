﻿using System.Collections.Generic;

namespace Spikes.SensorApp.Data
{
    public interface ISensorController
    {
        List<ISensor> Sensors { get; }
        ISensorTypeFilter Filter { get; }
        void ActivateSensorSystem();
    }
}