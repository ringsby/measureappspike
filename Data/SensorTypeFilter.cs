﻿using System.Collections.Generic;

namespace Spikes.SensorApp.Data
{
    public class SensorTypeFilter: ISensorTypeFilter
    {
        public SensorTypeFilter(IEnumerable<SensorSubscriptionType> sensorTypes)
        {
            SensorTypes = sensorTypes;
        }
        public IEnumerable<SensorSubscriptionType> SensorTypes { get; }
    }
}